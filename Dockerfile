FROM node:latest

WORKDIR /usr/src/app

RUN npm install
RUN npm i lerna -g --loglevel notice

COPY package.json .
COPY packages/backend ./packages/backend
COPY packages/telegramNotify ./packages/telegramNotify
COPY lerna.json .

RUN lerna bootstrap

CMD [ "npm", "run", "start-all"]
