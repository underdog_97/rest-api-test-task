import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import path from 'path';
import routes from './routes/index';
import dbConnection from './db/dbConnection';

dotenv.config({ path: path.resolve(__dirname, './config/.env') });

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/', routes);

dbConnection.dbConnect();

export default app;
