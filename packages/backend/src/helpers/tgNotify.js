import fs from 'fs';

const TelegramBot = require('node-telegram-bot-api');

const token = '1189560214:AAFg2Td5npSAGPD7sdid055flgzw-NLAoko';

export const bot = new TelegramBot(token, { polling: true });

export function sendToTg(chatId, text, tgHead) {
  tgHead.sendMessage(chatId, text);
}

bot.on('message', (msg) => {
  const chatId = msg.chat.id;
  if (msg.text === '/scan') {
    let chatsArray = [];

    fs.readFile('chats.txt', 'utf8',
      (error, data) => {
        chatsArray = data.split('\n');
        if (chatsArray.includes(chatId.toString()) === true) {
          bot.sendMessage(chatId, `${msg.from.first_name}, this chat already scanning requests!`);
        } else {
          fs.appendFile('chats.txt', `${chatId}\n`, (err) => {
            if (err) {
              throw err;
            }
            bot.sendMessage(chatId, `${msg.from.first_name}, requests information scanning for this chat!`);
          });
        }
      });
  } else if (msg.text === '/stop') {
    let chatsArray = [];

    fs.readFile('chats.txt', 'utf8',
      (error, data) => {
        if (error) throw error;
        chatsArray = data.split('\n');
        if (chatsArray.includes(chatId.toString()) === false) {
          bot.sendMessage(chatId, `${msg.from.first_name}, you do not track requests! Type /scan for tracking!`);
        } else {
          for (let i = 0; i < chatsArray.length; i += 1) {
            if (chatId.toString() === chatsArray[i]) {
              chatsArray.splice(i, 1);
              i = 0;
            }
          }

          fs.writeFile('chats.txt', chatsArray.join('\n'), (err) => {
            if (err) {
              throw err;
            }
            bot.sendMessage(chatId, `${msg.from.first_name}, you stopped scanning for this chat!`);
          });
        }
      });
  }
});
