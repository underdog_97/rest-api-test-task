import mongoose from 'mongoose';

const Schema = mongoose.Schema({
  name: String,
  surname: String,
  age: String,
  specification: String,
});

const Model = mongoose.model('Teacher', Schema);

export default {
  Model,
  Schema,
};
