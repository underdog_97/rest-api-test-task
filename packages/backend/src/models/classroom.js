import mongoose from 'mongoose';

const Schema = mongoose.Schema({
  name: String,
  places: String,
});

const Model = mongoose.model('Classroom', Schema);

export default {
  Model,
  Schema,
};
