import mongoose from 'mongoose';
import Student from './student';

const Schema = mongoose.Schema({
  name: String,
  members: [Student.Schema],
});

const Model = mongoose.model('Group', Schema);

export default {
  Model,
  Schema,
};
