import mongoose from 'mongoose';
import Teacher from './teacher';
import Classroom from './classroom';
import Group from './group';

const Schema = mongoose.Schema({
  theme: String,
  teacher: Teacher.Schema,
  classroom: Classroom.Schema,
  specification: String,
  pairNumber: String,
  group: Group.Schema,
});

const Model = mongoose.model('Lesson', Schema);

export default {
  Model,
  Schema,
};
