import mongoose from 'mongoose';

const Schema = mongoose.Schema({
  name: String,
  surname: String,
  age: String,
  phone: String,
  email: String,
});

const Model = mongoose.model('Student', Schema);

export default {
  Model,
  Schema,
};
