import Group from '../models/group';

export async function getAllGroups() {
  return Group.Model.find();
}

export async function getGroupById(id) {
  return Group.Model.findById(id);
}

export async function createGroup(group) {
  return (new Group.Model(group)).save();
}

export async function updateGroupById(id, group) {
  let findedGroup = await Group.Model.findOne({ _id: id });
  await Group.Model.updateOne({
    _id: id,
  },
  {
    name: group.name || findedGroup.name,
    members: group.members || findedGroup.members,
  });
  findedGroup = await Group.Model.findOne({ _id: id });
  return findedGroup;
}

export async function deleteGroupById(id) {
  return (Group.Model.deleteOne({ _id: id }));
}
