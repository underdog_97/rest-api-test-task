import Teacher from '../models/teacher';

export async function getAllTeachers() {
  return Teacher.Model.find();
}

export async function getTeacherById(id) {
  return Teacher.Model.findById(id);
}

export async function createTeacher(teacher) {
  return (new Teacher.Model(teacher)).save();
}

export async function updateTeacherById(id, teacher) {
  let findedTeacher = await Teacher.Model.findOne({ _id: id });
  await Teacher.Model.updateOne({
    _id: id,
  },
  {
    name: teacher.name || findedTeacher.name,
    surname: teacher.surname || findedTeacher.surname,
    age: teacher.age || findedTeacher.age,
    specification: teacher.specification || teacher.specification,
  });
  findedTeacher = await Teacher.Model.findOne({ _id: id });
  return findedTeacher;
}

export async function deleteTeacherById(id) {
  return (Teacher.Model.deleteOne({ _id: id }));
}
