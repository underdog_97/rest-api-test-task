import Classroom from '../models/classroom';

export async function getAllClassrooms() {
  return Classroom.Model.find();
}

export async function getClassroomById(id) {
  return Classroom.Model.findById(id);
}

export async function createClassroom(classroom) {
  return (new Classroom.Model(classroom)).save();
}

export async function updateClassroomById(id, classroom) {
  let findedClassroom = await Classroom.Model.findOne({ _id: id });
  await Classroom.Model.updateOne({
    _id: id,
  },
  {
    name: classroom.name || findedClassroom.name,
    places: classroom.surname || findedClassroom.surname,
  });
  findedClassroom = await Classroom.Model.findOne({ _id: id });
  return findedClassroom;
}

export async function deleteClassroomById(id) {
  return (Classroom.Model.deleteOne({ _id: id }));
}
