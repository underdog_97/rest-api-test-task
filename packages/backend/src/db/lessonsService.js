import Lesson from '../models/lesson';

export async function getAllLessons() {
  return Lesson.Model.find();
}

export async function getLessonById(id) {
  return Lesson.Model.findById(id);
}

export async function createLesson(lesson) {
  return (new Lesson.Model(lesson)).save();
}

export async function updateLessonById(id, lesson) {
  let findedLesson = await Lesson.Model.findOne({ _id: id });
  await Lesson.Model.updateOne({
    _id: id,
  },
  {
    theme: lesson.theme || findedLesson.theme,
    teacher: lesson.teacher || findedLesson.teacher,
    classroom: lesson.classroom || findedLesson.classroom,
    specification: lesson.specification || Lesson.specification,
    pairNumber: lesson.pairNumber || Lesson.pairNumber,
    group: lesson.group || Lesson.group,
  });
  findedLesson = await Lesson.Model.findOne({ _id: id });
  return findedLesson;
}

export async function deleteLessonById(id) {
  return (Lesson.Model.deleteOne({ _id: id }));
}
