import mongoose from 'mongoose';
import dotenv from 'dotenv';
import path from 'path';

dotenv.config({ path: path.resolve(__dirname, '../config/.env') });

const dbConnect = () => {
  mongoose.connect(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_DB}`, { useNewUrlParser: true }).then(() => {
    console.log('Connected to MongoDB.');
  }).catch((e) => {
    console.log(`Not Connected to Database ERROR:\n ${e}`);
  });
};

const closeConnect = () => {
  mongoose.connection.close().then(() => {
    console.log('Connection close.');
  }).catch((e) => {
    console.log(`Not closed. ERROR:\n', ${e}`);
  });
};

export default {
  dbConnect,
  closeConnect,
};
