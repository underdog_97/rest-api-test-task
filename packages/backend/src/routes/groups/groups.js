import express from 'express';
import {
  getAll, create, getById, updateById, deleteById,
} from '../../controllers/groups';
import { checkToken, verifyToken } from '../../middlewares/auth';
import tgNotify from '../../middlewares/telegramAlert';

const router = express.Router();

router.get('/', checkToken, verifyToken, getAll, tgNotify);
router.post('/create', checkToken, verifyToken, create, tgNotify);
router.get('/:id', checkToken, verifyToken, getById, tgNotify);
router.put('/:id', checkToken, verifyToken, updateById, tgNotify);
router.delete('/:id', checkToken, verifyToken, deleteById, tgNotify);

export default router;
