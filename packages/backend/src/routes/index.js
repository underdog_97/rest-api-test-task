import express from 'express';

import teachersRoutes from './teachers/teachers';
import classroomsRoutes from './classrooms/classrooms';
import groupsRoutes from './groups/groups';
import lessonsRoutes from './lessons/lessons';
import authRoutes from './auth/auth';

const router = express.Router();

router.use('/teachers', teachersRoutes);
router.use('/classrooms', classroomsRoutes);
router.use('/groups', groupsRoutes);
router.use('/lessons', lessonsRoutes);
router.use('/login', authRoutes);

export default router;
