import { sendToTg, bot } from '../helpers/tgNotify';

export default async (req, res, next) => {
  const d = new Date();
  const fullUrl = `${req.protocol}://${req.get('host')}${req.originalUrl}`;
  sendToTg('389024662', `[${d.getHours()}:${d.getMinutes()}] ${req.method} request to [${fullUrl}]`, bot);
  return next();
};
