import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import path from 'path';

dotenv.config({ path: path.resolve(__dirname, '../config/.env') });

export const checkToken = (req, res, next) => {
  const header = req.headers.authorization;

  if (typeof header !== 'undefined') {
    const bearer = header.split(' ');
    const token = bearer[1];

    req.token = token;
    return next();
  }
  throw res.send(401, 'Unatorized');
};

export const verifyToken = async (req, res, next) => {
  jwt.verify(req.token, process.env.JWT_TOKEN, (err) => {
    if (err) {
      throw res.send(403, 'Forbidden');
    } else {
      return next();
    }
  });
};
