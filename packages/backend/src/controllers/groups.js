import {
  getAllGroups,
  createGroup,
  updateGroupById,
  getGroupById,
  deleteGroupById,
} from '../db/groupsService';

export const getAll = async (req, res, next) => {
  const groups = await getAllGroups();
  res.status(200).json(groups);
  return next();
};

export const create = async (req, res, next) => {
  const group = await createGroup(req.body);
  res.status(201);
  res.send(group);
  return next();
};

export const getById = async (req, res, next) => {
  const group = await getGroupById(req.params.id);
  res.status(200);
  res.status(200).json(group);
  return next();
};

export const updateById = async (req, res, next) => {
  const group = await updateGroupById(req.params.id, req.body);
  res.status(200);
  res.status(200).json(group);
  return next();
};

export const deleteById = async (req, res, next) => {
  const group = await deleteGroupById(req.params.id);
  res.status(200);
  res.status(200).json(group);
  return next();
};
