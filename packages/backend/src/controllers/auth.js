import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import path from 'path';

dotenv.config({ path: path.resolve(__dirname, '../config/.env') });

const user = {
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
};

export default async (req, res) => {
  if (req.body.username === user.username && req.body.password === user.password) {
    jwt.sign({ user }, process.env.JWT_TOKEN, { expiresIn: '1h' }, (err, token) => {
      if (err) { throw err; }
      res.send(token);
    });
  } else {
    console.log('ERROR: Could not log in');
  }
};
