import {
  getAllClassrooms,
  createClassroom,
  updateClassroomById,
  getClassroomById,
  deleteClassroomById,
} from '../db/classroomsService';

export const getAll = async (req, res, next) => {
  const classrooms = await getAllClassrooms();
  res.status(200).json(classrooms);
  return next();
};

export const create = async (req, res, next) => {
  const classroom = await createClassroom(req.body);
  res.status(201);
  res.send(classroom);
  return next();
};

export const getById = async (req, res, next) => {
  const classroom = await getClassroomById(req.params.id);
  res.status(200);
  res.status(200).json(classroom);
  return next();
};

export const updateById = async (req, res, next) => {
  const classroom = await updateClassroomById(req.params.id, req.body);
  res.status(200);
  res.status(200).json(classroom);
  return next();
};

export const deleteById = async (req, res, next) => {
  const classroom = await deleteClassroomById(req.params.id);
  res.status(200);
  res.status(200).json(classroom);
  return next();
};
