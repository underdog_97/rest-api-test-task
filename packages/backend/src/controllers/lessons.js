import {
  getAllLessons,
  createLesson,
  updateLessonById,
  getLessonById,
  deleteLessonById,
} from '../db/lessonsService';

export const getAll = async (req, res, next) => {
  const lessons = await getAllLessons();
  res.status(200).json(lessons);
  return next();
};

export const create = async (req, res, next) => {
  const lesson = await createLesson(req.body);
  res.status(201);
  res.send(lesson);
  return next();
};

export const getById = async (req, res, next) => {
  const lesson = await getLessonById(req.params.id);
  res.status(200);
  res.status(200).json(lesson);
  return next();
};

export const updateById = async (req, res, next) => {
  const lesson = await updateLessonById(req.params.id, req.body);
  res.status(200);
  res.status(200).json(lesson);
  return next();
};

export const deleteById = async (req, res, next) => {
  const lesson = await deleteLessonById(req.params.id);
  res.status(200);
  res.status(200).json(lesson);
  return next();
};
