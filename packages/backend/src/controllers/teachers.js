import {
  getAllTeachers,
  createTeacher,
  updateTeacherById,
  getTeacherById,
  deleteTeacherById,
} from '../db/teachersService';

export const getAll = async (req, res, next) => {
  const teachers = await getAllTeachers();
  res.status(200).json(teachers);
  return next();
};

export const create = async (req, res, next) => {
  const teacher = await createTeacher(req.body);
  res.status(201);
  res.send(teacher);
  return next();
};

export const getById = async (req, res, next) => {
  const teacher = await getTeacherById(req.params.id);
  res.status(200);
  res.status(200).json(teacher);
  return next();
};

export const updateById = async (req, res, next) => {
  const teacher = await updateTeacherById(req.params.id, req.body);
  res.status(200);
  res.status(200).json(teacher);
  return next();
};

export const deleteById = async (req, res, next) => {
  const teacher = await deleteTeacherById(req.params.id);
  res.status(200);
  res.status(200).json(teacher);
  return next();
};
