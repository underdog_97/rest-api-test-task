Search "@ITUSE_bot" in telegram:
 - "/scan" in chat for start working
 - "/stop" in chat for stop working
 (The project must be started)

Copy your .env file in backend/src/config

Install Docker

In project root in terminal run npm i

In project root in terminal run cd packages/backend and npm i

In project root in terminal run docker-compose up

localhost:3000 to open backend